===========================
Stock User Scenario
===========================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Install stock Module::

    >>> config = activate_modules('stock_user')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

    >>> warehouse_loc2, = warehouse_loc.duplicate()

Add default warehouse to user::

    >>> User = Model.get('res.user')
    >>> user = User(config.user)
    >>> user.stock_warehouse = warehouse_loc
    >>> user.save()

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse == warehouse_loc
    True
    >>> shipment_out.company = company
    >>> shipment_out.save()

Create another Shipment Out::

    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc2
    >>> shipment_out.company = company
    >>> shipment_out.save()

Add warehouses to user and check filtering::

    >>> len(ShipmentOut.find([]))
    2
    >>> user.stock_warehouses.append(warehouse_loc)
    >>> user.save()
    >>> config._context = User.get_preferences(True, config.context)
    >>> config._context['stock_warehouses'] == [warehouse_loc.id]
    True
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc2
    >>> shipment_out.company = company
    >>> shipment_out.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Warehouse" in "Customer Shipment" is not valid according to its domain. - 